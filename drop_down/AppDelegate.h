//
//  AppDelegate.h
//  drop_down
//
//  Created by clicklabs124 on 10/6/15.
//  Copyright (c) 2015 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

