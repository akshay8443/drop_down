//
//  main.m
//  drop_down
//
//  Created by clicklabs124 on 10/6/15.
//  Copyright (c) 2015 akshay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
