//
//  ViewController.m
//  drop_down
//
//  Created by clicklabs124 on 10/6/15.
//  Copyright (c) 2015 akshay. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSArray *array1;
}

@property (weak, nonatomic) IBOutlet UIButton *button1;


@property (weak, nonatomic) IBOutlet UITableView *tableview1;


@end

@implementation ViewController
@synthesize tableview1;
@synthesize button1;
- (void)viewDidLoad {
    array1 = [NSArray new];
    array1 = @[@"akshay",@"koshal",@"param",@"vijay"];
   //  _button1.hidden=TRUE;
    tableview1.hidden=TRUE;
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(IBAction)btnPressed1{
    
   //_button1.hidden = FALSE;
      tableview1.hidden=FALSE;
   // [array1 addObject: tableview1];
   [tableview1 reloadData];
   // tableview1.text = @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;

{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array1.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuse" ];
    cell.textLabel.text = array1[indexPath.row];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *string=array1[indexPath.row];
    [button1 setTitle:string forState:UIControlStateNormal];
    tableview1.hidden=TRUE;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
